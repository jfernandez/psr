# psr
`psr` is a C++ library that generates triangle meshes from point data, specifically from the output of SPH simulations.

This project is divided into the library subproject `psr` and the app subproject `psr_app` which offers a command line executable.

## Command Line Executable
### Build and Compile
Use the standard CMake procedure. Here is an example for a UNIX system to build and compile the project in a folder inside the source code folder:
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```
The following command will display the help message from the `psr_app`:
```
./app/psr_app -h
```

### Basic usage
The only mandatory arguments are the path to the input file(s) (`--in`), the path to the output file(s) (`--out`) and the particle radius (`-r`).

`psr_app` can reconstruct one or multiple frames. 

Provide the name of a file containing point data to reconstruct that one frame:
```
./psr_app --in path/to/particles.vtk --out path/to/output_surface.vtk -r 0.011
```

For multiple frames, `psr_app` makes the assumption that all the file names are identical except for the iteration number which will always be at the end. To reconstruct a file sequence named (`particles_0.vtk`, `particles_1.vtk`, `particles_2.vtk`), just ignore the number at the end:
```
./psr_app --in particles_.vtk --out output_surface_.vtk --start_frame 0 --end_frame 2 -r 0.011
```

You can also use 
```
./psr_app --in particles_.vtk --out output_surface_.vtk --all -r 0.011
```
to reconstruct all files with tha name pattern.

Please, see the `--help` message to see other options and posibilities.
