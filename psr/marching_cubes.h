#pragma once
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <array>
#include <algorithm>
#include <unordered_map>
#include <iostream>
#include <omp.h>

// TODO: Should be properly included
#include "../extern/robin-map/include/tsl/robin_map.h"  

#include "SparseGrid.h"
#include "marching_cubes_lut.h"
#include "common.h"

namespace psr
{
	//struct TriMesh
	//{
	//	std::vector<Vector3f> vertices;
	//	std::vector<std::array<int, 3>> triangles;
	//};

	template<typename HashInt, typename Real>
	void sparseMarchingCubes(std::vector<Vector3f>& out_vertices, std::vector<std::array<int, 3>>& out_triangles, SparseGrid<HashInt>& grid, const tsl::robin_map<HashInt, Real>& values, const Real default_value)
	{
		// Initialize solution
		out_vertices.clear();
		out_triangles.clear();

		// Edge loop
		std::unordered_map<HashInt, int> edge_hash_to_vertex_idx;
		std::unordered_set<HashInt> relevant_cells;
		relevant_cells.reserve(values.size());

		//// For each node with negative level_set, check the edges that connect with it
		double t0 = omp_get_wtime();
		//std::cout << "sparseMarchingCubes edge loop started... ";
		for (const auto& key_value_pair : values) {

			// Skip the point if positive value
			if (key_value_pair.second > 0) {  // TODO: not sure about equality
				continue;
			}

			// Gather point data
			const HashInt point_hash = key_value_pair.first;
			const Real va = key_value_pair.second;
			const std::array<HashInt, 3> ijk = grid.getPointCoords(point_hash);

			/*
				Each point in the grid is connected to 6 edges. Positive and negative in each dimension.
			*/
			// Loop through edges that contain this point
			for (int dim = 0; dim < 3; dim++) {
				for (int sign = 0; sign < 2; sign++) {  // 0: negative, 1: positive

					/*
						Points in the limits of the grid do not have all edge neighbors
					*/
					// Neighbor out of bounds
					if (((sign == 0) && (ijk[dim] == 0)) || ((sign == 1) && (ijk[dim] == grid.np[dim] - 1))) {
						continue;
					}

					// Neighbor point grid information
					std::array<HashInt, 3> neigh_ijk = ijk;
					neigh_ijk[dim] += (sign == 0) ? -1 : 1;
					const HashInt neighbor_idx = grid.getPointHash(neigh_ijk[0], neigh_ijk[1], neigh_ijk[2]);

					// Fetch neighbor point value
					Real vb = 0.0;
					const auto it = values.find(neighbor_idx);
					if (it == values.end()) {
						vb = default_value;
					}
					else {
						vb = it->second;
					}

					// Edge intersection found
					if (va * vb <= 0.0) { // Different signs

						// Add the edge index to the list
						const HashInt edge_hash = grid.getEdgeHash(point_hash, dim);
						edge_hash_to_vertex_idx[edge_hash] = (int)out_vertices.size();

						// Interpolate the vertex with the values at the edge ends
						const Real alpha = va / (-vb + va);
						const Eigen::Vector3f vertex_a = grid.getPoint(ijk[0], ijk[1], ijk[2]);
						const Eigen::Vector3f vertex_b = grid.getPoint(neigh_ijk[0], neigh_ijk[1], neigh_ijk[2]);
						out_vertices.push_back((1.0 - alpha) * vertex_a + alpha * vertex_b);

						// Add cells that connectect to the edge to the relevant cell set
						std::array<HashInt, 4> cell_hashes;
						if (sign > 0) {
							cell_hashes = grid.getCellNeighborsOfEdge(ijk, neigh_ijk);
						}
						else {
							cell_hashes = grid.getCellNeighborsOfEdge(neigh_ijk, ijk);
						}

						for (const HashInt cell_hash : cell_hashes) {
							relevant_cells.insert(cell_hash);
						}

					}  // if edge intersects
				}  // Positive/Negative neighbors
			}  // Neighbor directions
		} // Negative valued points loop
		double t1 = omp_get_wtime();
		//std::cout << "finished.  " << t1 - t0 << " s." << std::endl;

		// Cell loop
		t0 = omp_get_wtime();
		//std::cout << "sparseMarchingCubes cell loop started... ";
		for (const HashInt cell_hash : relevant_cells) {

			// Cell grid information
			const std::array<HashInt, 3> cell_ijk = grid.getCellCoords(cell_hash);

			// Flag cell vertices with in/out
			std::array<HashInt, 8> cell_vertices_hashes;  // Will be reused later to assemble the triangles
			std::array<bool, 8> vertex_signs;
			for (int cell_vertex_i = 0; cell_vertex_i < 8; cell_vertex_i++) {
				const std::array<int, 3> cv = CELL_VERTICES[cell_vertex_i];

				/*
					cell_ijk are the ijk coordinates of the bottom corner vertex of the cell
				*/
				const std::array<HashInt, 3> point_ijk = {
					cell_ijk[0] + cv[0],
					cell_ijk[1] + cv[1],
					cell_ijk[2] + cv[2]
				};

				const HashInt point_hash = grid.getPointHash(point_ijk[0], point_ijk[1], point_ijk[2]);
				cell_vertices_hashes[cell_vertex_i] = point_hash;

				// If the vertes does not exists, use the default value
				Real vertex_value = 0.0;
				const auto it = values.find(point_hash);
				if (it == values.end()) {
					vertex_value = default_value;
				}
				else {
					vertex_value = it->second;
				}
				vertex_signs[cell_vertex_i] = vertex_value < 0.0;
			}

			// Query the marching cubes look up table for the corresponding triangulation
			const std::array<std::array<int, 3>, 5> triangles = getMarchingCubesCellTriangulation(vertex_signs);

			// Add triangles to the connectivity array of the mesh
			for (const std::array<int, 3>& triangle : triangles) {
				// From the look up table:  -1 means no more triangles
				if (triangle[0] == -1) {
					break;
				}

				// Transform local cell edge coordinates to global coordinates
				std::array<int, 3> triangle_vertices_indices = { -1, -1, -1 };

				/*
					Each triangle vertex is in a cell edge.
				*/
				//// Loop through cell edges that corresponds with the triangle vertices
				for (int loc_edge_i = 0; loc_edge_i < 3; loc_edge_i++) {

					// Cell edge local info
					const int loc_edge_idx = triangle[loc_edge_i];
					const std::array<int, 2> edge_vertices_loc_idx = CELL_EDGES[loc_edge_idx];

					// Global edge vertices info
					const std::array<HashInt, 2> edge_vertices_hashes = {
						cell_vertices_hashes[edge_vertices_loc_idx[0]],
						cell_vertices_hashes[edge_vertices_loc_idx[1]]
					};

					// Find edge hash
					const int edge_dir = CELL_EDGES_DIRECTION[loc_edge_idx];
					const HashInt min_edge_vertex_hash = std::min(edge_vertices_hashes[0], edge_vertices_hashes[1]);
					const HashInt edge_hash = grid.getEdgeHash(min_edge_vertex_hash, edge_dir);

					// If the node is not in the precomputed ones, add it (rare corner case)
					int tri_vertex_idx = -1;
					const auto it = edge_hash_to_vertex_idx.find(edge_hash);
					if (it == edge_hash_to_vertex_idx.end()) {
						// New point index
						tri_vertex_idx = (int)out_vertices.size();

						// Add the edge index to the list
						edge_hash_to_vertex_idx[edge_hash] = tri_vertex_idx;

						// Find the intersection point (analogous to before)
						std::array<Eigen::Vector3f, 2> edge_vertices;
						std::array<Real, 2> edge_values;
						for (HashInt loc_node_i = 0; loc_node_i < 2; loc_node_i++) {
							const HashInt point_hash = edge_vertices_hashes[loc_node_i];
							const std::array<HashInt, 3> ijk = grid.getPointCoords(point_hash);
							edge_vertices[loc_node_i] = grid.getPoint(ijk[0], ijk[1], ijk[2]);
							const auto it = values.find(point_hash);
							if (it == values.end()) {
								edge_values[loc_node_i] = default_value;
							}
							else {
								edge_values[loc_node_i] = it->second;
							}
						}
						const Real alpha = edge_values[0] / (-edge_values[1] + edge_values[0]);
						out_vertices.push_back((1.0 - alpha) * edge_vertices[0] + alpha * edge_vertices[1]);
					}
					else {
						tri_vertex_idx = it->second;
					}

					triangle_vertices_indices[loc_edge_i] = tri_vertex_idx;
				}  // Cell edge / triangle vertex loop

				out_triangles.push_back(triangle_vertices_indices);
			}  // Triangles loop

		}  // Cells loop
		t1 = omp_get_wtime();
		//std::cout << "finished.  " << t1 - t0 << " s." << std::endl;

		// Empty mesh error
		if (out_vertices.size() == 0) {
			std::cout << "psr error: Empty mesh reconstructed in marchingCubes()." << std::endl;
			abort();
		}
	};
}