#pragma once
#include <array>
#include <cassert>

#include "common.h"

namespace psr
{
	namespace kernel
	{
		constexpr float PI = 3.14159265358979323846f;
		namespace cubic
		{
			inline float spline(const float q)
			{
				assert(q >= 0.0f);
				constexpr float alpha = 3.0f / (2.0f * PI);

				if (q < 1.0f) {
					constexpr float a = 2.0f / 3.0f;
					return alpha * (a - q * q + 0.5f * q * q * q);
				}
				else if (q < 2.0f) {
					constexpr float a = 1.0f / 6.0f;
					const float b = 2.0f - q;
					return alpha * a * b * b * b;
				}
				else {
					return 0.0f;
				}
			};
			inline float W(const float distance_squared, const float h)
			{
				const float q = std::sqrt(distance_squared) / h;
				return spline(q) / (h * h * h);
			}
			inline float W(const Eigen::Vector3f& xi, const Eigen::Vector3f& xj, const float h)
			{
				const float q = (xi - xj).norm() / h;
				return spline(q) / (h * h * h);
			}
			inline float gradSpline(const float q)
			{
				assert(q >= 0.0f);
				constexpr float alpha = 3.0f / (2.0f * PI);

				if (q < 1.0f) {
					constexpr float a = 3.0f / 2.0f;
					return alpha * (-2.0f * q + a * q * q);
				}
				else if (q < 2.0f) {
					const float a = 2.0f - q;
					return alpha * (-0.5f * a * a);
				}
				else {
					return 0.0f;
				}
			};
			inline Eigen::Vector3f gradW(const Eigen::Vector3f& xi, const Eigen::Vector3f& xj, const float h)
			{
				const Eigen::Vector3f r = xi - xj;
				const float r_norm = r.norm();

				if (std::abs(r_norm) > 1e-10f) {
					const float q = r_norm / h;
					return gradSpline(q) / (h * h * h) * (r / r_norm / h);
				}
				else {
					// Not defined gradient if r_norm = 0.0
					return Eigen::Vector3f::Zero();
				}
			};
		}

		template<size_t N>
		class SquaredDistanceQuantization
		{
		public:
			// Fields
			float bin_size;
			std::array<float, N> quantization;

			// Methods
			SquaredDistanceQuantization(const float length, std::function<float(const float distance_squared)> f)
			{
				this->bin_size = length / N;
				for (int i = 0; i < N; i++) {
					const float r_squared = (0.5f + i) * this->bin_size;
					this->quantization[i] = f(r_squared);
				}
			}
			inline float evaluate_squared_distance(const float squared_distance)
			{
				const int quantile = (int)(squared_distance / this->bin_size);
				return this->quantization[quantile];
			}
		};
	};
};