#include "level_set_functions.h"

void psr::level_set::cubicSPHKernel(tsl::robin_map<LongKey, float>& output_level_set, const SparseGrid<LongKey>& grid, const std::vector<Vector3f>& particles, const std::vector<float>& particles_normalized_density, const float isovalue, const float smoothing_length)
{
	double t0 = omp_get_wtime();
	//std::cout << "level_set::cubicSPHKernel started... ";

	// Precomputation for the density estimation
	const float compact_support = 2.0f * smoothing_length;
	const float compact_support_squared = compact_support * compact_support;

	// Precompute a quantization for faster computation of the kernel function
	kernel::SquaredDistanceQuantization<100> cubic_quantization(compact_support_squared, 
		[smoothing_length](const float distance_squared) {
			return kernel::cubic::W(distance_squared, smoothing_length);
		});

	// Compute fluid particles contribution to grid nodes
	output_level_set.clear();
	output_level_set.reserve(particles.size());
	for (int particle_i = 0; particle_i < (int)particles.size(); particle_i++) {
		const float particle_vol = 1.0f / particles_normalized_density[particle_i];
		const Eigen::Vector3f particle = particles[particle_i];

		// Check
		if (!grid.isPointInternal(particle)) {
			std::cout << std::endl;
			std::cout << "Error: At least one particle's compact support is not fully contained in the SparseGrid." << std::endl;
			std::cout << "AABB min: " << grid.domain_internal.min().transpose() << std::endl;
			std::cout << "Particle: " << particle.transpose() << std::endl;
			std::cout << "AABB max: " << grid.domain_internal.max().transpose() << std::endl;
			exit(-1);
		}

		// Find subgrid that overlaps with the particle's compact support
		Eigen::AlignedBox<LongKey, 3> overlap_AABB = grid.overlapAABBWithCompactSupport(particle);
		const Eigen::Vector3f bottom_overlap = grid.getPoint(overlap_AABB.min()[0], overlap_AABB.min()[1], overlap_AABB.min()[2]);

		// Loop through the grid points which cells overlap with the particle's support radius
		//// Note: dx, dy, dz  are (grid_point - particle) coordinates
		//// Note2: This is the bottleneck of the whole surface reconstruction
		//// TODO: We could load a precomputed subgrid and just scatter the solution to the hashmap
		//// TODO2: In the recursive dense version, we could update with precomputed AVX lines
		float dx = bottom_overlap[0] - grid.cell_size - particle[0];
		for (LongKey i = overlap_AABB.min()[0]; i < overlap_AABB.max()[0]; i++) {
			dx += grid.cell_size;
			const float dx2 = dx * dx;

			float dy = bottom_overlap[1] - grid.cell_size - particle[1];
			for (LongKey j = overlap_AABB.min()[1]; j < overlap_AABB.max()[1]; j++) {
				dy += grid.cell_size;
				const float dy2 = dy * dy;

				float dz = bottom_overlap[2] - grid.cell_size - particle[2];
				for (LongKey k = overlap_AABB.min()[2]; k < overlap_AABB.max()[2]; k++) {
					dz += grid.cell_size;
					const float dz2 = dz * dz;


					// If the grid points is actually inside the particle's support radius, add contribution
					const float r_squared = dx2 + dy2 + dz2;
					if (r_squared < compact_support_squared) {
						const float contribution = particle_vol * cubic_quantization.evaluate_squared_distance(r_squared);

						// Check that the point exists
						const LongKey grid_point_hash = grid.getPointHash(i, j, k);
						auto it = output_level_set.find(grid_point_hash);  // Bottleneck: HashMap insertions
						if (it == output_level_set.end()) {
							output_level_set[grid_point_hash] = contribution - isovalue;
						}
						else {
							it.value() += contribution;
						}
					}
				}
			}
		}
	}
	double t1 = omp_get_wtime();
	//std::cout << "finished.  " << t1 - t0 << " s." << std::endl;
}

void psr::level_set::solenthaler(tsl::robin_map<LongKey, float>& output_level_set, const SparseGrid<LongKey>& grid, const std::vector<Vector3f>& particles, const float particle_radius, const float compact_support, const float t_low, const float t_high)
{
	double t0 = omp_get_wtime();
	std::cout << "level_set::solenthaler started... ";

	// Precomputation
	const float compact_support_squared = compact_support * compact_support;
	const float inv_particle_radius = 1.0f / particle_radius;

	// Compute fluid particles contribution to grid nodes
	std::unordered_map<LongKey, solenthaler::LevelSetData> level_set_data;
	level_set_data.reserve(particles.size());
	for (int particle_i = 0; particle_i < (int)particles.size(); particle_i++) {
		const Eigen::Vector3f particle = particles[particle_i];

		// Check
		if (!grid.isPointInternal(particle)) {
			std::cout << std::endl;
			std::cout << "Error: At least one particle's compact support is not fully contained in the SparseGrid." << std::endl;
			std::cout << "AABB min: " << grid.domain_internal.min().transpose() << std::endl;
			std::cout << "Particle: " << particle.transpose() << std::endl;
			std::cout << "AABB max: " << grid.domain_internal.max().transpose() << std::endl;
			exit(-1);
		}

		// Find subgrid that overlaps with the particle's compact support
		Eigen::AlignedBox<LongKey, 3> overlap_AABB = grid.overlapAABBWithCompactSupport(particle);
		const Eigen::Vector3f bottom_overlap = grid.getPoint(overlap_AABB.min()[0], overlap_AABB.min()[1], overlap_AABB.min()[2]);

		// Loop through the grid points which cells overlap with the particle's support radius
		for (LongKey i = overlap_AABB.min()[0]; i < overlap_AABB.max()[0]; i++) {
			for (LongKey j = overlap_AABB.min()[1]; j < overlap_AABB.max()[1]; j++) {
				for (LongKey k = overlap_AABB.min()[2]; k < overlap_AABB.max()[2]; k++) {

					// Compute Solenthaler kernel and its gradient
					const Eigen::Vector3f grid_point = grid.getPoint(i, j, k);
					const Eigen::Vector3f r = grid_point - particle;
					const float innerKernel = 1 - r.squaredNorm() * inv_particle_radius * inv_particle_radius;
					const float w = innerKernel * innerKernel * innerKernel;
					const float kernelDerivativeFactor = (-6.0f) * innerKernel * innerKernel * inv_particle_radius * inv_particle_radius;
					Eigen::Vector3f grad_w;
					grad_w.x() = (grid_point.x() - particle.x()) * kernelDerivativeFactor;
					grad_w.y() = (grid_point.y() - particle.y()) * kernelDerivativeFactor;
					grad_w.z() = (grid_point.z() - particle.z()) * kernelDerivativeFactor;

					// Update the level set data
					const LongKey grid_point_hash = grid.getPointHash(i, j, k);
					auto& data = level_set_data[grid_point_hash];
					data.v += w;
					data.u += particle * w;
					data.dv += grad_w;
					data.du.col(0) += particle * grad_w.x();
					data.du.col(1) += particle * grad_w.y();
					data.du.col(2) += particle * grad_w.z();
				}
			}
		}
	}

	output_level_set.reserve(particles.size());
	for (auto& pair : level_set_data) {
		const auto& hash = pair.first;
		const auto& data = pair.second;
		const auto& ijk = grid.getPointCoords(hash);
		output_level_set[hash] = solenthaler::level_set(grid.getPoint(ijk[0], ijk[1], ijk[2]), data, t_low, t_high, particle_radius);
	}

	double t1 = omp_get_wtime();
	std::cout << "finished.  " << t1 - t0 << " s." << std::endl;
}
