#pragma once
#include <vector>
#include <array>

#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include "level_set_functions.h"
#include "sph_functions.h"
#include "marching_cubes.h"
#include "SparseGrid.h"
#include "common.h"


namespace psr
{
	/* 
		This class does the same as psr::reconstruct_cubicSPHKernel_sparse but it reuses all the buffers
		for extra performance when many frames are computed.
	*/
	class ThreadLevelSetReconstruction
	{
	public:
		/* Fields */
		// Output mesh
		std::vector<Vector3f> particles;
		std::vector<Vector3f> vertices;
		std::vector<std::array<int, 3>> triangles;
		std::vector<Vector3f> normals;
		
		// Simulation
		float smoothing_length;
		float compact_support;
		float cell_size;
		float isovalue;

		// Mid data
		std::vector<float> particles_contiguous;
		std::vector<float> particle_normalized_density;
		CompactNSearch::NeighborhoodSearch cns;
		unsigned int fluid_set_id;
		kernel::SquaredDistanceQuantization<100> cubic_quantization;
		tsl::robin_map<LongKey, float> level_set;
		OpenMesh::TriMesh_ArrayKernelT<> openmesh;

		/* Methods */
		ThreadLevelSetReconstruction(const float smoothing_length, const float cell_size, const float isovalue);
		~ThreadLevelSetReconstruction() = default;

		void reconstruct();
	};
}