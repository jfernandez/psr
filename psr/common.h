#pragma once
#include <Eigen/Dense>


namespace psr
{
	constexpr float MAX_FLOAT = std::numeric_limits<float>::max();
	constexpr float MIN_FLOAT = -std::numeric_limits<float>::max();

	using Vector3f = Eigen::Matrix<float, 3, 1, Eigen::DontAlign>;
}