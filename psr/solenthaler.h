#pragma once
#include <vector>
#include <array>

#include "../extern/robin-map/include/tsl/robin_map.h"  

#include "common.h"
#include "kernel.h"
#include "SparseGrid.h"

namespace psr
{
	namespace solenthaler
	{
		struct LevelSetData
		{
			float v = 0.0f;
			Eigen::Vector3f u;
			Eigen::Vector3f dv;
			Eigen::Matrix3f du;
			LevelSetData() { u.setZero(); dv.setZero(); du.setZero(); };
		};
		float level_set(const Eigen::Vector3f& position, const LevelSetData& data, const float t_low, const float t_high, const float particle_radius);
	}
}