#pragma once
#include <iostream>
#include <vector>
#include <array>

#include <Eigen/Dense>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Tools/Decimater/DecimaterT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>
#include <OpenMesh/Tools/Decimater/ModHausdorffT.hh>
#include <OpenMesh/Tools/Decimater/ModNormalDeviationT.hh>
#include <OpenMesh/Tools/Decimater/ModRoundnessT.hh>
#include <OpenMesh/Tools/Smoother/JacobiLaplaceSmootherT.hh>
#include <OpenMesh/Tools/Smoother/SmootherT.hh>

#include "../extern/robin-map/include/tsl/robin_set.h"  

#include "common.h"

namespace psr
{
	void compute_node_normals(std::vector<psr::Vector3f>& out_normals, const std::vector<psr::Vector3f>& vertices, const std::vector<std::array<int, 3>>& triangles);
	void make_openmesh(OpenMesh::TriMesh_ArrayKernelT<>& mesh, const std::vector<psr::Vector3f>& vertices, const std::vector<std::array<int, 3>>& triangles);
	void openmesh_smooth(OpenMesh::TriMesh_ArrayKernelT<>& mesh, std::vector<psr::Vector3f>& vertices, const int iterations);
	void openmesh_decimate_normal_deviation(OpenMesh::TriMesh_ArrayKernelT<> &mesh, std::vector<psr::Vector3f>& vertices, std::vector<std::array<int, 3>>& triangles, const float normal_deviation);

	bool is_watertight(const std::vector<std::array<int, 3>>& triangles, tsl::robin_set<uint64_t> &buffer);
}