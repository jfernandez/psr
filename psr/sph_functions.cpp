#include "sph_functions.h"

std::vector<float> psr::compute_particle_normalized_density(const std::vector<Vector3f>& particles, const float smoothing_length)
{
	double t0 = omp_get_wtime();
	std::cout << "compute_particle_normalized_density started... ";

	// Allocate solution
	std::vector<float> particles_normalized_density(particles.size());

	// Since we use here the cubic kernel
	const float compact_support = 2.0f * smoothing_length;


	// Use CompactNSearch for the neighborhood search
	//// Arange the positions contiguous in memory
	std::vector<CompactNSearch::Real> positions;
	positions.reserve(3 * particles.size());
	for (auto& particle : particles) {
		positions.push_back(CompactNSearch::Real(particle[0]));
		positions.push_back(CompactNSearch::Real(particle[1]));
		positions.push_back(CompactNSearch::Real(particle[2]));
	}

	//// Run CompactNSearch
	auto compact_n_search = CompactNSearch::NeighborhoodSearch(compact_support);
	auto fluid_set_id = compact_n_search.add_point_set(positions.data(), particles.size());
	compact_n_search.set_active(fluid_set_id, fluid_set_id, true);
	compact_n_search.find_neighbors();
	auto& fluid_set = compact_n_search.point_set(fluid_set_id);

	// Compute the normalized density
	for (int particle_i = 0; particle_i < (int)particles.size(); particle_i++) {
		particles_normalized_density[particle_i] = kernel::cubic::W(particles[particle_i], particles[particle_i], smoothing_length); // Central particle
		for (auto i = 0; i < fluid_set.n_neighbors(fluid_set_id, particle_i); i++) {
			const int global_neighbor_idx = fluid_set.neighbor(fluid_set_id, particle_i, i);
			particles_normalized_density[particle_i] += kernel::cubic::W(particles[particle_i], particles[global_neighbor_idx], smoothing_length);
		}
	}

	double t1 = omp_get_wtime();
	std::cout << "finished.  " << t1 - t0 << " s." << std::endl;
	return particles_normalized_density;
}
