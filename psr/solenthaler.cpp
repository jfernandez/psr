#include "solenthaler.h"

float psr::solenthaler::level_set(const Eigen::Vector3f& position, const LevelSetData& data, const float t_low, const float t_high, const float particle_radius)
{
	const Eigen::Vector3f weightedAvg = data.u / data.v;

	// Jacobian
	Eigen::Matrix3f jacobian;
	jacobian(0, 0) = data.du(0, 0) * data.v - data.u.x() * data.dv.x();
	jacobian(1, 0) = data.du(1, 0) * data.v - data.u.y() * data.dv.x();
	jacobian(2, 0) = data.du(2, 0) * data.v - data.u.z() * data.dv.x();

	jacobian(0, 1) = data.du(0, 1) * data.v - data.u.x() * data.dv.y();
	jacobian(1, 1) = data.du(1, 1) * data.v - data.u.y() * data.dv.y();
	jacobian(2, 1) = data.du(2, 1) * data.v - data.u.z() * data.dv.y();

	jacobian(0, 2) = data.du(0, 2) * data.v - data.u.x() * data.dv.z();
	jacobian(1, 2) = data.du(1, 2) * data.v - data.u.y() * data.dv.z();
	jacobian(2, 2) = data.du(2, 2) * data.v - data.u.z() * data.dv.z();

	jacobian /= (data.v * data.v);

	// Calculate f
	const Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> solver(jacobian);
	const float EV_max = solver.eigenvalues().cwiseAbs().maxCoeff();
	float f = 1.0;
	if (EV_max >= t_low && EV_max <= t_high)
	{
		const float gamma = (t_high - EV_max) / (t_high - t_low);
		f = std::pow(gamma, 3) - 3 * std::pow(gamma, 2) + 3 * gamma;
	}
	else if (EV_max > t_high) {
		f = 0;
	}

	// Level set
	const float distance = (position - weightedAvg).norm() - particle_radius * f;
	return distance;
}
