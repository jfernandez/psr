#pragma once
#include <vector>
#include <array>
#include <limits>
#include <iostream>
#include <stdint.h>

#include "common.h"


namespace psr
{
	using LongKey = uint64_t;


	// NOTE: This is duplicated from the marching cubes table. 
	// Although maybe is good to keep things disconnected
	constexpr std::array< std::array<int, 3>, 8> CELL_VERTICES = { {
		{ { 0, 0, 0 } }, // 0
		{ { 1, 0, 0 } }, // 1
		{ { 1, 1, 0 } }, // 2
		{ { 0, 1, 0 } }, // 3
		{ { 0, 0, 1 } }, // 4
		{ { 1, 0, 1 } }, // 5
		{ { 1, 1, 1 } }, // 6
		{ { 0, 1, 1 } }  // 7
		}
	};

	template<typename KeyInt>
	class SparseGrid
	{
	public:
		/* Fields */
		Eigen::AlignedBox3f domain;
		Eigen::AlignedBox3f domain_internal;
		float cell_size;
		KeyInt n_cells_in_compact_support_radius;
		std::array<KeyInt, 3> nc = { 0, 0, 0 };  // n_cells_per_dimension
		std::array<KeyInt, 3> np = { 0, 0, 0 };  // n_points_per_dimension
		KeyInt n_points;
		KeyInt n_edges;

		/* Methods */
		SparseGrid(const Vector3f& bottom, const std::array<int, 3>& n_cells_per_dimension, const float cell_size, const float compact_support);
		static SparseGrid<KeyInt> generateGridThatContains(const std::vector<Vector3f> &particles, const float cell_size, const float compact_support);

		static bool hashesFitIn32Bits(const Vector3f& bottom, const std::array<int, 3>& n_cells_per_dimension, const float cell_size);
		KeyInt getPointHash(const KeyInt i, const KeyInt j, const KeyInt k) const;
		KeyInt getCellHash(const KeyInt i, const KeyInt j, const KeyInt k) const;
		KeyInt getEdgeHash(const KeyInt point_hash, const KeyInt dir) const;
		std::array<KeyInt, 3> getCellCoordsAt(const Vector3f& x) const;
		std::array<KeyInt, 3> getPointCoords(const KeyInt point_hash) const;
		std::array<KeyInt, 3> getCellCoords(const KeyInt cell_hash) const;
		Vector3f getPoint(const KeyInt i, const KeyInt j, const KeyInt k) const;
		std::array<KeyInt, 4> getCellNeighborsOfEdge(const std::array<KeyInt, 3>& min_ijk, const std::array<KeyInt, 3>& max_ijk) const;
		bool isPointInternal(const Vector3f &point) const;
		Eigen::AlignedBox<KeyInt, 3> overlapAABBWithCompactSupport(const Vector3f& point) const;
	};





	template<typename KeyInt>
	inline SparseGrid<KeyInt>::SparseGrid(const Vector3f& bottom, const std::array<int, 3>& n_cells_per_dimension, const float cell_size, const float compact_support)
	{
		// Check that the grid fits in the scope of uint64
		if ((float)std::numeric_limits<KeyInt>::max() < (float)n_cells_per_dimension[0] * (float)n_cells_per_dimension[1] * (float)n_cells_per_dimension[2]) {
			std::cout << "psr::SparseGrid error: grid too large to hash." << std::endl;
			abort();
		}

		// Assignment
		this->nc = { (KeyInt)n_cells_per_dimension[0], (KeyInt)n_cells_per_dimension[1], (KeyInt)n_cells_per_dimension[2] };
		this->cell_size = cell_size;
		this->domain = Eigen::AlignedBox3f(bottom, bottom + cell_size * Vector3f((float)this->nc[0], (float)this->nc[1], (float)this->nc[2]));
		const Vector3f margin = compact_support * Vector3f::Ones();
		this->domain_internal = Eigen::AlignedBox3f(this->domain.min() + margin, this->domain.max() - margin);
		this->n_cells_in_compact_support_radius = (KeyInt)(compact_support / this->cell_size) + 1;

		// Precomputation
		this->np = { this->nc[0] + 1, this->nc[1] + 1, this->nc[2] + 1 };
		this->n_points = (KeyInt)(this->np[0] * this->np[1] * this->np[2]);
		this->n_edges = 3 * this->n_points;
	}

	template<typename KeyInt>
	inline bool SparseGrid<KeyInt>::hashesFitIn32Bits(const Vector3f& bottom, const std::array<int, 3>& n_cells_per_dimension, const float cell_size)
	{
		const float max_32bit_hash = std::pow(static_cast<float>(2.0), 32);
		const float max_hash = (float)SparseGrid<KeyInt>(bottom, n_cells_per_dimension, cell_size, 0.0).n_edges + 3;
		return max_hash < max_32bit_hash;
	}
	template<typename KeyInt>
	inline SparseGrid<KeyInt> SparseGrid<KeyInt>::generateGridThatContains(const std::vector<Vector3f>& particles, const float cell_size, const float compact_support)
	{
		if (particles.size() == 0) {
			std::cout << "Error: particle list is empty." << std::endl;
			exit(-1);
		}

		// Find the minimal AABB that contains all particles
		Eigen::AlignedBox3f domain(particles[0]);
		for (auto& particle : particles) {
			domain.extend(particle);
		}

		// Enlarge the AABB to account that particle's compact support must be contained as well
		const Vector3f margin = 2.001f * compact_support * Vector3f::Ones();
		Eigen::AlignedBox3f domain_enlarged(domain.min() - margin, domain.max() + margin);
		
		// Find the number of cells per dimension for the enlarged AABB
		const Vector3f diagonal = domain_enlarged.diagonal();
		std::array<int, 3> n_cells_per_dimension;
		for (int dim = 0; dim < 3; dim++) {
			n_cells_per_dimension[dim] = std::max(1, (int)(diagonal[dim] / cell_size) + 1);
		}

		return SparseGrid<KeyInt>(domain_enlarged.min(), n_cells_per_dimension, cell_size, compact_support);
	}
	template<typename KeyInt>
	inline KeyInt SparseGrid<KeyInt>::getPointHash(const KeyInt i, const KeyInt j, const KeyInt k) const
	{
		return i * this->np[1] * this->np[2] + j * this->np[2] + k;
	}
	template<typename KeyInt>
	inline KeyInt SparseGrid<KeyInt>::getCellHash(const KeyInt i, const KeyInt j, const KeyInt k) const
	{
		return i * this->nc[1] * this->nc[2] + j * this->nc[2] + k;
	}
	template<typename KeyInt>
	inline KeyInt SparseGrid<KeyInt>::getEdgeHash(const KeyInt point_hash, const KeyInt dir) const
	{
		return 3 * point_hash + dir;
	}
	template<typename KeyInt>
	inline std::array<KeyInt, 3> SparseGrid<KeyInt>::getCellCoordsAt(const Vector3f& x) const
	{
		const Vector3f offset = x - this->domain.min();
		return { (KeyInt)(offset[0] / this->cell_size), (KeyInt)(offset[1] / this->cell_size), (KeyInt)(offset[2] / this->cell_size) };
	}
	template<typename KeyInt>
	inline std::array<KeyInt, 3> SparseGrid<KeyInt>::getPointCoords(const KeyInt point_hash) const
	{
		const KeyInt i = point_hash / (this->np[1] * this->np[2]);
		const KeyInt j = (point_hash - i * this->np[1] * this->np[2]) / this->np[2];
		const KeyInt k = point_hash - i * this->np[1] * this->np[2] - j * this->np[2];
		return { i, j, k };
	}
	template<typename KeyInt>
	inline std::array<KeyInt, 3> SparseGrid<KeyInt>::getCellCoords(const KeyInt cell_hash) const
	{
		const KeyInt i = cell_hash / (this->nc[1] * this->nc[2]);
		const KeyInt j = (cell_hash - i * this->nc[1] * this->nc[2]) / this->nc[2];
		const KeyInt k = cell_hash - i * this->nc[1] * this->nc[2] - j * this->nc[2];
		return { i, j, k };
	}
	template<typename KeyInt>
	inline Vector3f SparseGrid<KeyInt>::getPoint(const KeyInt i, const KeyInt j, const KeyInt k) const
	{
		return this->domain.min() + Vector3f(i * this->cell_size, j * this->cell_size, k * this->cell_size);
	}
	template<typename KeyInt>
	inline std::array<KeyInt, 4> SparseGrid<KeyInt>::getCellNeighborsOfEdge(const std::array<KeyInt, 3>& min_ijk, const std::array<KeyInt, 3>& max_ijk) const
	{
		// Note: This could be done faster (maybe) with a look up table but I think is overkilling

		// Initialize
		std::array<KeyInt, 4> edge_cell_hashes = { 0, 0, 0, 0 };

		// Find 8 cells connected to each node
		const std::array<std::array<KeyInt, 3>, 2> ijks = { min_ijk, max_ijk };
		std::array<std::array<KeyInt, 8>, 2> points_cell_hashes;
		for (int point_i = 0; point_i < 2; point_i++) {  // Edge points
			for (int vertex_i = 0; vertex_i < 8; vertex_i++) {  // Cell vertices
				const std::array<int, 3> cv = CELL_VERTICES[vertex_i];
				const std::array<KeyInt, 3> cell_ijk = {
					ijks[point_i][0] - cv[0],
					ijks[point_i][1] - cv[1],
					ijks[point_i][2] - cv[2]
				};
				points_cell_hashes[point_i][vertex_i] = this->getCellHash(cell_ijk[0], cell_ijk[1], cell_ijk[2]);
			}
		}

		// Add the hashes that are in both points lists
		int counter = 0;
		for (int vertex_i = 0; vertex_i < 8; vertex_i++) {
			for (int vertex_j = 0; vertex_j < 8; vertex_j++) {
				if (points_cell_hashes[0][vertex_i] == points_cell_hashes[1][vertex_j]) {
					edge_cell_hashes[counter] = points_cell_hashes[0][vertex_i];
					counter++;
					break;
				}
			}

			if (counter == 4) {
				break;
			}
		}

		return edge_cell_hashes;
	}
	template<typename KeyInt>
	inline bool SparseGrid<KeyInt>::isPointInternal(const Vector3f& point) const
	{
		return this->domain_internal.contains(point);
	}
	template<typename KeyInt>
	inline Eigen::AlignedBox<KeyInt, 3> SparseGrid<KeyInt>::overlapAABBWithCompactSupport(const Vector3f& point) const
	{
		const std::array<LongKey, 3> center_cell_ijk = this->getCellCoordsAt(point);
		Eigen::AlignedBox<KeyInt, 3> overlap_AABB;
		overlap_AABB.min() = {
			center_cell_ijk[0] - n_cells_in_compact_support_radius,
			center_cell_ijk[1] - n_cells_in_compact_support_radius,
			center_cell_ijk[2] - n_cells_in_compact_support_radius
		};
		const KeyInt overlap_length = n_cells_in_compact_support_radius * 2 + 1;  // One more positive than negative
		overlap_AABB.max() = {
			overlap_AABB.min()[0] + overlap_length,
			overlap_AABB.min()[1] + overlap_length,
			overlap_AABB.min()[2] + overlap_length
		};

		return overlap_AABB;
	}
}