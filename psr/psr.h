#pragma once
#include <vector>
#include <array>

#include "level_set_functions.h"
#include "sph_functions.h"
#include "marching_cubes.h"
#include "SparseGrid.h"
#include "common.h"

namespace psr
{
	void reconstruct_cubicSPHKernel_sparse(
		std::vector<Vector3f>& out_vertices,
		std::vector<std::array<int, 3>>& out_triangles,
		const std::vector<Vector3f>& particles,
		const float smoothing_length,
		const float cell_size,
		const float isovalue
	);
	void reconstruct_solenthaler_sparse(
		std::vector<Vector3f>& out_vertices,
		std::vector<std::array<int, 3>>& out_triangles,
		const std::vector<Vector3f>& particles,
		const float particle_radius,
		const float compact_support, const float cell_size,
		const float t_low, const float t_high
	);


}