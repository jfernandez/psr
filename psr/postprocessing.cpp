#include "postprocessing.h"

void psr::compute_node_normals(std::vector<psr::Vector3f>& out_normals, const std::vector<psr::Vector3f>& vertices, const std::vector<std::array<int, 3>>& triangles)
{
	out_normals.clear();
	out_normals.resize(vertices.size(), psr::Vector3f::Zero());
	for (const std::array<int, 3> &triangle : triangles) {
		const psr::Vector3f& p0 = vertices[triangle[0]];
		const psr::Vector3f& p1 = vertices[triangle[1]];
		const psr::Vector3f& p2 = vertices[triangle[2]];

		const psr::Vector3f triangle_normal = (p0 - p2).cross(p1 - p2);
		const double triangle_area = 0.5 * std::abs((p1[0] - p0[0]) * (p2[1] - p0[1]) - (p2[0] - p0[0]) * (p1[1] - p0[1]));

		out_normals[triangle[0]] += triangle_area * triangle_normal;
		out_normals[triangle[1]] += triangle_area * triangle_normal;
		out_normals[triangle[2]] += triangle_area * triangle_normal;
	}

	for (psr::Vector3f& normal : out_normals) {
		normal.normalize();
	}
}

void psr::make_openmesh(OpenMesh::TriMesh_ArrayKernelT<>& mesh, const std::vector<psr::Vector3f>& vertices, const std::vector<std::array<int, 3>>& triangles)
{
	mesh.clean_keep_reservation();
	for (const psr::Vector3f& p : vertices) {
		mesh.add_vertex(OpenMesh::TriMesh_ArrayKernelT<>::Point(p[0], p[1], p[2]));
	}

	for (const std::array<int, 3>&tri : triangles) {
		mesh.add_face(mesh.vertex_handle(tri[0]), mesh.vertex_handle(tri[1]), mesh.vertex_handle(tri[2]));
	}
}

void psr::openmesh_smooth(OpenMesh::TriMesh_ArrayKernelT<>& mesh, std::vector<psr::Vector3f>& vertices, const int iterations)
{
	typedef OpenMesh::TriMesh_ArrayKernelT<> Mesh;

	OpenMesh::Smoother::JacobiLaplaceSmootherT<Mesh> smoother(mesh);
	smoother.initialize(
		OpenMesh::Smoother::SmootherT<Mesh>::Component::Tangential_and_Normal,   //Smooth direction
		OpenMesh::Smoother::SmootherT<Mesh>::Continuity::C1);                    //Continuity
	smoother.smooth(iterations);

	for (auto& v : mesh.vertices()) {
		auto p = mesh.point(v);
		vertices[v.idx()] = { p[0], p[1], p[2] };
	}
}

void psr::openmesh_decimate_normal_deviation(OpenMesh::TriMesh_ArrayKernelT<>& mesh, std::vector<psr::Vector3f>& vertices, std::vector<std::array<int, 3>>& triangles, const float normal_deviation)
{
	typedef OpenMesh::TriMesh_ArrayKernelT<> Mesh;

	// Note: All methods in OpenMesh are bad. ModNormalDeviationT is the least bad.
	// ModQuadricT is kind of OK but leads to triangle inversion.

	OpenMesh::Decimater::DecimaterT<Mesh>   decimater(mesh);  // a decimater object, connected to a mesh
	//OpenMesh::Decimater::ModQuadricT<Mesh>::Handle method;      // use a quadric module
	OpenMesh::Decimater::ModNormalDeviationT<Mesh>::Handle method;      // use a quadric module
	decimater.add(method); // register module at the decimater

	//decimater.module(method).set_max_err(1e-8);
	decimater.module(method).set_normal_deviation(normal_deviation);
	decimater.module(method).set_binary(false);
	decimater.initialize();
	decimater.decimate();
	// after decimation: remove decimated elements from the mesh
	mesh.delete_isolated_vertices();
	mesh.garbage_collection();

	// Get the mesh back
	vertices.clear();
	for (auto& v : mesh.vertices()) {
		auto p = mesh.point(v);
		vertices.push_back({ p[0], p[1], p[2] });
	}

	triangles.clear();
	for (auto& f : mesh.faces()) {
		auto face_vertex_iter = mesh.fv_begin(f);
		std::array<int, 3> triangle;
		for (int i = 0; i < 3; i++) {
			triangle[i] = (*face_vertex_iter).idx();
			face_vertex_iter++;
		}
		triangles.push_back(triangle);
	}
}

bool psr::is_watertight(const std::vector<std::array<int, 3>>& triangles, tsl::robin_set<uint64_t>& buffer)
{
	std::array<std::array<int, 2>, 3> edges = { 0, 1, 1, 2, 2, 0 };
	const uint64_t max_edge_idx = 3 * (uint64_t)triangles.size();

	buffer.clear();
	for (const std::array<int, 3> &triangle : triangles) {
		for (const std::array<int, 2> &edge_loc : edges) {
			const std::array<int, 2> edge = {
				std::min(triangle[edge_loc[0]], triangle[edge_loc[1]]), 
				std::max(triangle[edge_loc[0]], triangle[edge_loc[1]])
			};
			const uint64_t edge_idx = edge[0]*max_edge_idx + edge[1];

			auto it = buffer.find(edge_idx);
			if (it == buffer.end()) {
				buffer.insert(edge_idx);
			}
			else {
				buffer.erase(edge_idx);
			}
		}
	}

	return buffer.size() == 0;
}
