#include "ThreadLevelSetReconstruction.h"

psr::ThreadLevelSetReconstruction::ThreadLevelSetReconstruction(const float smoothing_length, const float cell_size, const float isovalue)
	: cell_size(cell_size), isovalue(isovalue), smoothing_length(smoothing_length), cns(2.0 * smoothing_length), cubic_quantization(0.0f, [](float h) { return 0.0; })
{
	this->compact_support = 2.0f * smoothing_length;
	const float compact_support_squared = compact_support * compact_support;

	this->cubic_quantization = kernel::SquaredDistanceQuantization<100>(compact_support_squared,
		[smoothing_length](const float distance_squared) {
			return kernel::cubic::W(distance_squared, smoothing_length);
		});
}

void psr::ThreadLevelSetReconstruction::reconstruct()
{
	// Compute particle_normalized_density
	bool restart_cns = 3*particles.size() != this->particles_contiguous.size();

	//// Neighborhood search
	////// Particles to buffer
	this->particles_contiguous.resize(3*particles.size());
	for (size_t i = 0; i < particles.size(); i++) {
		this->particles_contiguous[3 * i + 0] = particles[i][0];
		this->particles_contiguous[3 * i + 1] = particles[i][1];
		this->particles_contiguous[3 * i + 2] = particles[i][2];
	}

	if (restart_cns) {
		this->cns = CompactNSearch::NeighborhoodSearch(this->compact_support);
		this->fluid_set_id = this->cns.add_point_set(this->particles_contiguous.data(), particles.size());
		this->cns.set_active(fluid_set_id, fluid_set_id, true);
	}
	cns.find_neighbors();
	auto& fluid_set = cns.point_set(this->fluid_set_id);

	//// Compute the normalized density
	this->particle_normalized_density.resize(this->particles.size());
	const float init_val = cubic_quantization.evaluate_squared_distance(0.0f);
	for (int particle_i = 0; particle_i < (int)particles.size(); particle_i++) {
		const Vector3f& pi = particles[particle_i];

		this->particle_normalized_density[particle_i] = init_val; // Central particle
		for (auto i = 0; i < fluid_set.n_neighbors(this->fluid_set_id, particle_i); i++) {
			const unsigned int global_neighbor_idx = fluid_set.neighbor(this->fluid_set_id, particle_i, i);
			const Vector3f& pj = particles[global_neighbor_idx];
			this->particle_normalized_density[particle_i] += cubic_quantization.evaluate_squared_distance((pi - pj).squaredNorm());
		}
	}

	// Initialize the marching cubes sparse grid
	SparseGrid<LongKey> grid = SparseGrid<LongKey>::generateGridThatContains(particles, cell_size, compact_support);
	level_set::cubicSPHKernel(level_set, grid, particles, particle_normalized_density, isovalue, smoothing_length);

	// Sparse marching cubes
	const float background_default_value = -isovalue; // Non-found values are outside
	sparseMarchingCubes<LongKey, float>(this->vertices, this->triangles, grid, level_set, background_default_value);
}
