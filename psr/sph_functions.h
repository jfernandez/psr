#pragma once
#include <vector>
#include <omp.h>

#include "common.h"
#include "kernel.h"
#include "../extern/CompactNSearch/include/CompactNSearch/CompactNSearch.h"

namespace psr
{
	std::vector<float> compute_particle_normalized_density(const std::vector<Vector3f>& particles, const float smoothing_length);
}