#include "psr.h"

void psr::reconstruct_cubicSPHKernel_sparse(std::vector<Vector3f>& out_vertices, std::vector<std::array<int, 3>>& out_triangles, const std::vector<Vector3f>& particles, const float smoothing_length, const float cell_size, const float isovalue)
{
	// Precomputations
	const float compact_support = 2.0f * smoothing_length; // Only when using cubic kernel
	
	// Initialize the marching cubes sparse grid
	SparseGrid<LongKey> grid = SparseGrid<LongKey>::generateGridThatContains(particles, cell_size, compact_support);

	// Compute particle_normalized_density
	std::vector<float> particle_normalized_density = compute_particle_normalized_density(particles, smoothing_length);

	// Compute fluid particles contribution to the grid nodes
	tsl::robin_map<LongKey, float> level_set;
	level_set::cubicSPHKernel(level_set, grid, particles, particle_normalized_density, isovalue, smoothing_length);

	// Sparse marching cubes
	const float background_default_value = -isovalue; // Non-found values are outside
	sparseMarchingCubes<LongKey, float>(out_vertices, out_triangles, grid, level_set, background_default_value);
}

void psr::reconstruct_solenthaler_sparse(std::vector<Vector3f>& out_vertices, std::vector<std::array<int, 3>>& out_triangles, const std::vector<Vector3f>& particles, const float particle_radius, const float compact_support, const float cell_size, const float t_low, const float t_high)
{
	std::cout << "psr error: Solenthaler is not working!" << std::endl;
	exit(-1);

	// Initialize the marching cubes sparse grid
	SparseGrid<LongKey> grid = SparseGrid<LongKey>::generateGridThatContains(particles, cell_size, compact_support);

	// Compute fluid particles contribution to the grid nodes
	tsl::robin_map<LongKey, float> level_set;
	level_set::solenthaler(level_set, grid, particles, particle_radius, compact_support, t_low, t_high);

	// Sparse marching cubes
	const float background_default_value = -particle_radius; // Non-found values are outside
	sparseMarchingCubes<LongKey, float>(out_vertices, out_triangles, grid, level_set, background_default_value);
}
