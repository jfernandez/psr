#pragma once
#include <vector>
#include <array>

#include "../extern/robin-map/include/tsl/robin_map.h"  

#include "common.h"
#include "kernel.h"
#include "SparseGrid.h"
#include "solenthaler.h"

namespace psr
{
	namespace level_set
	{
		void cubicSPHKernel(
			tsl::robin_map<LongKey, float>& output_level_set,
			const SparseGrid<LongKey>& grid,
			const std::vector<Vector3f>& particles,
			const std::vector<float>& particles_normalized_density,
			const float isovalue,
			const float smoothing_length
		);
		void solenthaler(
			tsl::robin_map<LongKey, float>& output_level_set,
			const SparseGrid<LongKey>& grid,
			const std::vector<Vector3f>& particles,
			const float particle_radius, 
			const float compact_support,
			const float t_low, 
			const float t_high
		);
	}
}