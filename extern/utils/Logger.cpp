#include "Logger.h"

void stb::utils::Logger::start_timing(const std::string label)
{
	this->t0[label] = omp_get_wtime();
}

void stb::utils::Logger::stop_timing(const std::string label)
{
	const double t1 = omp_get_wtime();
	auto it = this->t0.find(label);
	if (it == this->t0.end()) {
		std::cout << "Error: Label (" << label << ") not found in Timing.t0." << std::endl;
		exit(-1);
	}
	const double t0 = this->t0[label];
	this->series[label].push_back(std::to_string(t1 - t0));
}

void stb::utils::Logger::save_to_disk(const std::string path)
{
	// Open the file
	std::ofstream outfile(path);
	if (!outfile) {
		std::cout << "Cannot open a file " << path << std::endl;
		exit(-1);
	}

	for (auto& pair : this->series) {
		outfile << pair.first << ": ";
		for (std::string& v : pair.second) {
			outfile << v << ", ";
		}
		outfile << std::endl;
	}
	outfile.close();
}
