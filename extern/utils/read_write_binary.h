#pragma once
#include <vector>
#include <string>
#include <fstream>
#include <iostream>


namespace stb
{
	namespace utils
	{
		template<typename T>
		void read_binary(std::vector<T> &output, const std::string path)
		{
			std::ifstream file(path, std::ios::binary);
			if (!file) {
				std::cout << "Error: Cannot read " << path << std::endl;
				exit(-1);
			}

			std::streampos fsize = 0;

			fsize = file.tellg();
			file.seekg(0, std::ios::end);
			fsize = file.tellg() - fsize;
			file.seekg(0, std::ios::beg);

			const int n = (int)fsize / sizeof(T);
			output.resize(n);
			file.read(reinterpret_cast<char*>(output.data()), n * sizeof(T));

			file.close();
		}

		template<typename T>
		void write_binary(const std::vector<T>& data, const std::string path)
		{
			std::ofstream file(path, std::ios::binary);
			if (!file) {
				std::cout << "Error: Cannot read " << path << std::endl;
				exit(-1);
			}
			file.write(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(T));
			file.close();
		}
	}
}