#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <omp.h>

namespace stb
{
	namespace utils
	{
		class Logger
		{
		public:
			std::unordered_map<std::string, std::vector<std::string>> series;
			std::unordered_map<std::string, double> t0;

			void start_timing(const std::string label);
			void stop_timing(const std::string label);
			void save_to_disk(const std::string path);
			template<typename T>
			void add(const std::string label, const T v)
			{
				this->series[label].push_back(std::to_string(v));
			};
		};
	}
}