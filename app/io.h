#include <vector>
#include <string>

#include <Partio.h>
#include "../extern/vtkio/VTKFile.h"
#include "../extern/happly.h"
#include "../extern/utils/FileSystem.h"
#include "../psr/common.h"
using psr::Vector3f;

namespace io
{
	// Read
	void read_particles(std::vector<Vector3f> &points, const std::string path, const Eigen::AlignedBox3f& aabb, const float ghost_distance = 0.0f);
	void read_particles_from_vtk(std::vector<Vector3f> &points, const std::string path);
	void read_particles_from_vtk(std::vector<Vector3f> &points, const std::string path, const Eigen::AlignedBox3f& aabb, const float ghost_distance = 0.0f);
	void read_particles_from_bgeo(std::vector<Vector3f> &points, const std::string path);
	void read_particles_from_bgeo(std::vector<Vector3f> &points, const std::string path, const Eigen::AlignedBox3f& aabb, const float ghost_distance = 0.0f);

	// Write
	void write_trimesh(const std::string path, const std::vector<Vector3f>& vertices, const std::vector<std::array<int, 3>>& triangles, const std::vector<Vector3f>& normals);
	void write_trimesh_to_vtk(const std::string path, const std::vector<Vector3f>& vertices, const std::vector<std::array<int, 3>>& triangles, const std::vector<Vector3f>& normals);
	void write_trimesh_to_ply(const std::string path, const std::vector<Vector3f>& vertices, const std::vector<std::array<int, 3>>& triangles);
}

