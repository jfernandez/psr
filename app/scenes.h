#pragma once
#include <vector>
#include <array>
#include <string>
#include <fstream>  // Read and write files
#include <iostream>
#include <numeric>
#include <omp.h>  // Just for timing

#include "../psr/psr.h"
using psr::Vector3f;

#include "io.h"


void damBreak();
void canyon();
void double_dam_break();
void double_dam_break_solenthaler();
void double_dam_break_171k();