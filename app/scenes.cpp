#include "scenes.h"

void damBreak()
{
	//const std::vector<Vector3f> particles = io::read_particles_from_vtk("../res/dam_break_particles.vtk");
	//const float particle_radius = 0.0564627f;
	//const float smoothing_length = 1.2f * 2.0f * particle_radius;
	//const float cell_size = 1.0f * particle_radius;
	//const float isovalue = 0.55f;
	//
	//psr::TriMesh tri_mesh = psr::reconstruct_cubicSPHKernel_sparse(particles, smoothing_length, cell_size, isovalue);
	//io::write_trimesh_to_vtk("../res/dam_break.vtk", tri_mesh.vertices, tri_mesh.triangles);
}

void canyon()
{
	//// Input
	//const float particle_radius = 0.011f;
	//const float cell_size = 1.25f * particle_radius;
	//const float isovalue = 0.55f;
	//const bool window = false;

	//// Input processing
	//Eigen::AlignedBox3f domain;
	//std::string out_path;
	//if (window) {
	//	domain = Eigen::AlignedBox3f(Vector3f(-4.5f, -1.7f, 3.45f), Vector3f(6.8f, 0.67f, 11.5f));
	//	out_path = "../res/canyon_window.vtk";
	//}
	//else {
	//	domain = Eigen::AlignedBox3f(Vector3f(-8.5f, -5.1f, -25.5f), Vector3f(12.0f, 3.1f, 30.5f));
	//	out_path = "../res/canyon.vtk";
	//}

	//// Load 
	//const std::vector<Vector3f> particles = io::read_particles_from_vtk("../res/canyon_particles.vtk", domain, 0.0f);

	//// Run
	//const float smoothing_length = 1.2f * 2.0f * particle_radius;
	//psr::TriMesh tri_mesh = psr::reconstruct_cubicSPHKernel_sparse(particles, smoothing_length, cell_size, isovalue);
	//io::write_trimesh_to_vtk(out_path, tri_mesh.vertices, tri_mesh.triangles);
	//std::cout << "Output saved to " << out_path << std::endl;
}

void double_dam_break()
{
	//// Input
	//const float particle_radius = 0.011f;
	//const float cell_size = 1.25f * particle_radius;
	//const float isovalue = 0.55f;
	//const bool window = false;
	//
	//// Input processing
	//Eigen::AlignedBox3f domain;
	//std::string out_path;
	//if (window) {
	//	domain = Eigen::AlignedBox3f(Vector3f(-3.1f, -0.1f, -3.1f), Vector3f(-2.1f, 0.9f, -2.1f));
	//	out_path = "../res/ddb_window.ply";
	//}
	//else {
	//	domain = Eigen::AlignedBox3f(Vector3f(-3.1f, -0.1f, -3.1f), Vector3f(3.1f, 9.5f, 3.1f));
	//	out_path = "../res/ddb.ply";
	//}

	//// Load 
	//const std::vector<Vector3f> particles = io::read_particles_from_bgeo("../res/ddb_particles.bgeo", domain, 0.0f);

	//// Run
	//const float smoothing_length = 1.2f * 2.0f * particle_radius;
	//psr::TriMesh tri_mesh = psr::reconstruct_cubicSPHKernel_sparse(particles, smoothing_length, cell_size, isovalue);
	//io::write_trimesh_to_ply(out_path, tri_mesh.vertices, tri_mesh.triangles);
	//std::cout << "Output saved to " << out_path << std::endl;
}

void double_dam_break_solenthaler()
{
	//// Input
	//const float particle_radius = 0.011f;
	//const float cell_size = 1.25f * particle_radius;
	//const bool window = true;

	//// Input processing
	//Eigen::AlignedBox3f domain;
	//std::string out_path;
	//if (window) {
	//	domain = Eigen::AlignedBox3f(Vector3f(-3.1f, -0.1f, -3.1f), Vector3f(-2.1f, 0.9f, -2.1f));
	//	out_path = "../res/ddb_solen_window.ply";
	//}
	//else {
	//	domain = Eigen::AlignedBox3f(Vector3f(-3.1f, -0.1f, -3.1f), Vector3f(3.1f, 9.5f, 3.1f));
	//	out_path = "../res/ddb_solen.ply";
	//}

	//// Load 
	//const std::vector<Vector3f> particles = io::read_particles_from_bgeo("../res/ddb_particles.bgeo", domain, 0.0f);

	//// Run
	//const float smoothing_length = 1.2f * 2.0f * particle_radius;
	//psr::TriMesh tri_mesh = psr::reconstruct_solenthaler_sparse(particles, particle_radius, 2.0f * smoothing_length, cell_size, 0.4f, 4.0f);
	//io::write_trimesh_to_ply(out_path, tri_mesh.vertices, tri_mesh.triangles);
	//std::cout << "Output saved to " << out_path << std::endl;
}

void double_dam_break_171k()
{
	//// Input
	//const float particle_radius = 0.010f;
	//const float cell_size = 1.25f * particle_radius;
	//const float isovalue = 0.55f;
	//Eigen::AlignedBox3f domain = Eigen::AlignedBox3f(Vector3f(-1.6f, -0.1f, -1.6f), Vector3f(1.6f, 3.1f, 1.6f));

	//// Load 
	//const std::vector<Vector3f> particles = io::read_particles_from_vtk("../res/170k_113.vtk", domain, 0.0f);

	//// Run
	//const float smoothing_length = 1.2f * 2.0f * particle_radius;
	//psr::TriMesh tri_mesh = psr::reconstruct_cubicSPHKernel_sparse(particles, smoothing_length, cell_size, isovalue);
	//io::write_trimesh_to_vtk("../res/ddb_171.vtk", tri_mesh.vertices, tri_mesh.triangles);
	//io::write_trimesh_to_ply("../res/ddb_171.ply", tri_mesh.vertices, tri_mesh.triangles);

	//vtkio::VTKStream vtk_stream;
	//vtk_stream.setPointsNestedIterator(particles.begin(), particles.end());

	//std::vector<int> idxs(particles.size());
	//std::iota(idxs.begin(), idxs.end(), 0);
	//vtk_stream.setCells(idxs.begin(), idxs.end(), vtkio::tables::CellType::Vertex);
	//vtk_stream.setPointData("id", idxs.begin(), idxs.end(), vtkio::tables::AttributeType::Scalars);
	//vtk_stream.write("../res/particles_171.vtk");
}
