#include <iostream>
#include <vector>
#include <array>
#include <string>
#include <omp.h>  // Just for timing

#include <Eigen/Dense>

#include "../utils/FileSystem.h"
#include "../cxxopts.hpp"

#include "../psr/psr.h"
#include "../psr/postprocessing.h"
#include "../psr/ThreadLevelSetReconstruction.h"
#include "io.h"

struct Options
{
    // Path
    std::string in;
    std::string out;
    int start_frame = -1;
    int end_frame = std::numeric_limits<int>::max();
    bool reconstruct_all = false;
    std::string current_directory;
    std::string in_extension;
    std::string out_extension;
    std::string in_path;
    std::string out_path;
    std::string in_name;
    std::string out_name;

    // Reconstruction
    float particle_radius = -1.0f;
    float smoothing_length = -1.0f;
    float cell_size = -1.0f;
    float isovalue = -1.0f;
    float smoothing_length_multiplier = -1.0f;
    float cell_size_multiplier = -1.0f;

    // Post-processing
    int smoothing_iterations = -1;
    float decimation_normal_deviation = -1.0f;

    // Misc
    Eigen::AlignedBox3f window;
    bool geometric_normals = false;
    int n_threads = -1;


    Options()
    {
        this->window.setEmpty();
    };

    void print()
    {
        std::cout << " ========== psr ========== " << std::endl;
        std::cout << "in: " << in << std::endl;
        std::cout << "out: " << out << std::endl;
        std::cout << "current working directory: " << current_directory << std::endl;
        std::cout << std::endl;
        std::cout << "particle_radius: " << particle_radius << std::endl;
        std::cout << "smoothing_length: " << smoothing_length << std::endl;
        std::cout << "cell_size: " << cell_size << std::endl;
        std::cout << "isovalue: " << isovalue << std::endl;
        std::cout << std::endl;
        std::cout << "n_threads: " << n_threads << std::endl;
        if (!window.isEmpty())
            std::cout << "window: (" << window.min().transpose() << ") | (" << window.max().transpose() << ")" << std::endl;
        if (smoothing_iterations > 0)
            std::cout << "smoothing_iterations: " << smoothing_iterations << std::endl;
        if (decimation_normal_deviation > 0)
            std::cout << "decimation_normal_deviation: " << decimation_normal_deviation << std::endl;
        std::cout << std::endl;
    };
};

void reconstruct_frame(const std::string in, const std::string out, const Options &opts, psr::ThreadLevelSetReconstruction& rec)
{
    // Load
    io::read_particles(rec.particles, in, opts.window);

    // Run
    rec.reconstruct();

    // Post-process
    if (opts.smoothing_iterations > 0 || opts.decimation_normal_deviation > 0) {
        psr::make_openmesh(rec.openmesh, rec.vertices, rec.triangles);
        if (opts.smoothing_iterations > 0) {
            psr::openmesh_smooth(rec.openmesh, rec.vertices, opts.smoothing_iterations);
        }
        if (opts.decimation_normal_deviation > 0) {
            psr::openmesh_decimate_normal_deviation(rec.openmesh, rec.vertices, rec.triangles, opts.decimation_normal_deviation);
        }
    }
    if (opts.geometric_normals) {
        psr::compute_node_normals(rec.normals, rec.vertices, rec.triangles);
    }

    // Save
    io::write_trimesh(out, rec.vertices, rec.triangles, rec.normals);

    std::cout << stb::utils::FileSystem::getFileName(out) << std::endl;
}

int main(int argc, const char* argv[])
{
    // Reconstruction options
    Options opts;

    // Parse command line arguments
    try
    {
        cxxopts::Options options(argv[0], "psr: Particle Surface Reconstruction (development in progress)");
        options
            .positional_help("[optional args]")
            .show_positional_help();

        options
            //.allow_unrecognised_options()
            .add_options()

            // File specifications
            ("in", "particle file input (.vtk or .bgeo)", cxxopts::value<std::string>(opts.in))
            ("out", "triangle mesh file output (.vtk or .ply)", cxxopts::value<std::string>(opts.out))
            ("all", "render all frames in the folder")
            ("start_frame", "start frame (ignore if one frame)", cxxopts::value<int>(opts.start_frame))
            ("end_frame", "start frame (ignore if one frame)", cxxopts::value<int>(opts.end_frame))

            // Description
            ("r,particle_radius", "particle radius", cxxopts::value<float>(opts.particle_radius))
            ("v,isovalue", "isosurface value", cxxopts::value<float>(opts.isovalue)->default_value("0.55f"))
            ("l,smoothing_length", "smoothing length multiplier to the particle diameter", cxxopts::value<float>(opts.smoothing_length_multiplier)->default_value("1.2f"))
            ("s,grid_size", "grid cell size in relation to the particle radius", cxxopts::value<float>(opts.cell_size_multiplier)->default_value("1.25f"))
            ("w,window", "AABB to restrict the reconstruction in (Example: '0,0,0,1,1,1')", cxxopts::value<std::vector<float>>())

            // Method

            // Post processing
            ("smoothing_iterations", "Smoothing iterations to apply in in OpenMesh::JacobiLaplaceSmootherT. (suggested: 5)", cxxopts::value<int>(opts.smoothing_iterations))
            ("decimation_deviation", "Normal deviation to apply in OpenMesh::ModNormalDeviationT. (suggested: 8)", cxxopts::value<float>(opts.decimation_normal_deviation))

            // Other
            ("geometric_normals", "Output with the geometric normals")
            ("n_threads", "Number of threads for multiframe", cxxopts::value<int>(opts.n_threads))
            ("h,help", "print help");

        auto command_line = options.parse(argc, argv);

        if (command_line.count("help")) {
            std::cout << options.help({ "", "Group" }) << std::endl;
            std::cout << "Mandatory arguments are '--in', '--out' and '--particle_radius'" << std::endl;
            std::cout << std::endl;
            std::cout << "Examples:" << std::endl;
            std::cout << "\t One frame:    ./psr --in path/to/particles.vtk --out path/to/output_surface.vtk -r 0.011" << std::endl;
            std::cout << "\t All frames:   ./psr --in particles_.vtk --out output_surface_.vtk --all -r 0.011" << std::endl;
            std::cout << "\t Window:       ./psr --in particles.vtk --out output_surface.vtk -r 0.011 --window -1.0,-1.0,-1.0,1.0,1.0,1.0" << std::endl;
            std::cout << "\t Postprocess:  ./psr --in particles.vtk --out output_surface.vtk -r 0.011 --smoothing_iterations 5 --decimation_deviation 10" << std::endl;
            exit(0);
        }

        if (command_line.count("window")) {
            const auto values = command_line["window"].as<std::vector<float>>();
            opts.window.min() = { values[0], values[1], values[2] };
            opts.window.max() = { values[3], values[4], values[5] };
        }

        if (command_line.count("all")) {
            opts.reconstruct_all = true;
        }
        if (command_line.count("geometric_normals")) {
            opts.geometric_normals = true;
        }
    }
    catch (const cxxopts::OptionException& e)
    {
        std::cout << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }

    // TODO
    std::cout << "SPECIFY MANDATORY ARGS" << std::endl;
    std::cout << "PRINT EXAMPLES" << std::endl;

    // Input checking
    if (opts.in.size() == 0) {
        std::cout << "psr input error: No input file(s) specified." << std::endl;
        exit(-1);
    }
    if (opts.out.size() == 0) {
        std::cout << "psr output error: No out file(s) specified." << std::endl;
        exit(-1);
    }
    if (opts.particle_radius == -1.0f) {
        std::cout << "psr input error: No particle radius specified." << std::endl;
        exit(-1);
    }

    // Process input
    if (opts.n_threads == -1) {
        opts.n_threads = omp_get_num_procs();
    }
    opts.smoothing_length = opts.smoothing_length_multiplier * 2.0f * opts.particle_radius;
    opts.cell_size = opts.cell_size_multiplier * opts.particle_radius;

    //// Path stuff
    opts.current_directory = stb::utils::FileSystem::getCurrentDir();
    opts.in = stb::utils::FileSystem::normalizePath(opts.current_directory + "/" + opts.in);
    opts.out = stb::utils::FileSystem::normalizePath(opts.current_directory + "/" + opts.out);
    opts.in_extension = stb::utils::FileSystem::getFileExt(opts.in);
    opts.out_extension = stb::utils::FileSystem::getFileExt(opts.out);
    opts.in_path = stb::utils::FileSystem::getFilePath(opts.in);
    opts.out_path = stb::utils::FileSystem::getFilePath(opts.out);
    opts.in_name = opts.in_path + "/" + stb::utils::FileSystem::getFileName(opts.in);
    opts.out_name = opts.out_path + "/" + stb::utils::FileSystem::getFileName(opts.out);

    //// All?
    if (opts.reconstruct_all) {
        std::vector<std::string> files;
        stb::utils::FileSystem::getFilesInDirectory(opts.in_path, files);
        opts.start_frame = 0;
        opts.end_frame = (int)files.size();
    }

    // Check
    if (opts.in_extension != "vtk" && opts.in_extension != "bgeo") {
        std::cout << "psr input error: Input file(s) must have exension .vtk or .bgeo." << std::endl;
        exit(-1);
    }
    if (opts.out_extension != "vtk" && opts.out_extension != "ply") {
        std::cout << "psr output error: Output file(s) must have exension .vtk or .ply." << std::endl;
        exit(-1);
    }
    if (opts.out_extension == "ply" && opts.geometric_normals) {
        std::cout << "psr output error: Normal output is only available in vtk format." << std::endl;
        exit(-1);
    }

    opts.print();
    const double t0 = omp_get_wtime();

    // Just one frame
    if (opts.start_frame == -1) {
        psr::ThreadLevelSetReconstruction rec(opts.smoothing_length, opts.cell_size, opts.isovalue);
        reconstruct_frame(opts.in, opts.out, opts, rec);
    }

    // Multiple frames in parallel
    else {
        opts.end_frame++; // Ensures inclusive range
        #pragma omp parallel num_threads(opts.n_threads)
        {
            psr::ThreadLevelSetReconstruction rec(opts.smoothing_length, opts.cell_size, opts.isovalue);

            #pragma omp for schedule(dynamic)
            for (int it = opts.start_frame; it < opts.end_frame; it++) {

                const std::string in_i = opts.in_name + std::to_string(it) + "." + opts.in_extension;
                const std::string out_i = opts.out_name + std::to_string(it) + "." + opts.out_extension;

                if (!stb::utils::FileSystem::fileExists(in_i)) {
                    continue;
                }

                reconstruct_frame(in_i, out_i, opts, rec);
            }
        }
    }

    const double t1 = omp_get_wtime();
    std::cout << "psr total execution time: " << t1 - t0 << " s" << std::endl;

    return 0;
}